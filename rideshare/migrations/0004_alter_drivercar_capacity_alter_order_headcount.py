# Generated by Django 4.0.1 on 2022-01-29 15:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rideshare', '0003_alter_order_user'),
    ]

    operations = [
        migrations.AlterField(
            model_name='drivercar',
            name='capacity',
            field=models.PositiveIntegerField(),
        ),
        migrations.AlterField(
            model_name='order',
            name='headcount',
            field=models.PositiveIntegerField(),
        ),
    ]
